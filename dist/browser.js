const browsersync = require('browser-sync');
const wpd = require('./wpd');

/** Launch the browser */
module.exports = () => {
  const browserOpts = {
    proxy: 'localhost:8000',
    files: []
  };
  if(wpd.hasTheme) {
    browserOpts.files.push(wpd.themeDirectory);
  }
  browsersync.init(browserOpts);
};
