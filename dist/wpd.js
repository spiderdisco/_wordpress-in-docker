const path = require('path');
const fs = require('fs');
const { safeString } = require('./utils');
const out = require('./out');

class WpdInfo {
  constructor() {
    this.themeName = undefined;
  }

  get directory() {
    return process.cwd();
  }

  get name() {
    return path.basename(this.directory);
  }

  get configFile() {
    return path.join(this.directory, '.wpd.json');
  }

  get isInitialised() {
    return fs.existsSync(this.configFile);
  }

  get hasTheme() {
    return this.themeName !== undefined;
  }

  get themeDirectory() {
    return this.hasTheme ? path.join(this.directory, this.safeThemeName) : undefined;
  }

  get safeThemeName() {
    return this.hasTheme ? safeString(this.themeName) : undefined;
  }

  save() {
    fs.writeFileSync(this.configFile, JSON.stringify(this));
  }
}

const wpd = new WpdInfo();
if(wpd.isInitialised) {
  out.tmi('Reading config...');
  const config = JSON.parse(fs.readFileSync(wpd.configFile));
  wpd.themeName = config.themeName;
}

module.exports = wpd;
