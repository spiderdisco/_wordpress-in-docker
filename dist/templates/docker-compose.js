const wpd = require('../wpd');

module.exports = () => {
  // let volumes = `volumes:
  //     - ./.data/wp/:/var/www/html/
  //     ${wpd.hasTheme ? `- ./${wpd.safeThemeName}:/${wpd.safeThemeName}` : ''}
  // `;

  const themeVolume = wpd.hasTheme ? `- ./${wpd.safeThemeName}:/${wpd.safeThemeName}` : '';

  // if(wpd.themeName) {
  //   volumes = `volumes:
  //     - ./${wpd.safeThemeName}:/${wpd.safeThemeName}`;
  // }

  return `
version: '3.3'

services:
  db:
    container_name: "${wpd.name}_database"
    image: mysql:5.7
    volumes:
      - ./.data/db/:/var/lib/mysql/
    restart: on-failure
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    container_name: "${wpd.name}_wordpress"
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: on-failure
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    volumes:
      - .data/wp/:/var/www/html/
      ${themeVolume}
`;
};
