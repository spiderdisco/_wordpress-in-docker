const fs = require('fs');
const path = require('path');
const browser = require('./browser');

const docker = require('./docker');
const out = require('./out');
const wpd = require('./wpd');

module.exports = () => {
  out.info('Initialising...');
  console.log(`dir: ${wpd.directory}`);
  console.log(`name: ${wpd.name}`);
  console.log(`theme: ${wpd.themeName}`);

  out.info('Creating project files...');
  // Create docker-compose.yml
  fs.writeFileSync(
    path.join(wpd.directory, 'docker-compose.yml'),
    require('./templates/docker-compose')()
  );

  fs.appendFileSync(path.join(wpd.directory, '.gitignore'),
    '# wordpress-in-docker\n.data/\nwp_export/\ndump.sql\n'
  );

  // Create theme
  if(wpd.hasTheme) {
    if(!fs.existsSync(wpd.themeDirectory)) {
      fs.mkdirSync(wpd.themeDirectory);
      fs.writeFileSync(
        path.join(wpd.themeDirectory, 'style.css'),
        require('./templates/style')()
      );
      fs.writeFileSync(
        path.join(wpd.themeDirectory, 'index.php'),
        '<?php ?>',
      );
    }
  }

  out.info('Bringing up Docker containers...');
  // Bring up Docker containers, and initialise.
  docker.up().then(() => {
    out.info('Setting up Docker containers...');

    // Append "define('FS_METHOD','direct');" to wp-config.php.
    // Stops WordPress asking for FTP access when installing to or uploading from the wp-admin.
    // docker.execWP('echo "define(\'FS_METHOD\',\'direct\');" >> wp-config.php');
    docker.wpConfigDefine('FS_METHOD', 'direct');


    // SymLink the theme directory bound to ./theme into wp-content/themes.
    // Can't put it directly into wp-content/themes, as it seems to break permissions.
    if(wpd.hasTheme) {
      docker.execWP(`ln -s /${wpd.safeThemeName} /var/www/html/wp-content/themes`);
    }

    // Install wp-cli
    out.info('Installing wp-cli...');
    docker.execWP('curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar');
    docker.execWP('chmod +x wp-cli.phar');
    docker.execWP('mv wp-cli.phar /usr/local/bin/wp', true);

    out.info('Configuring WordPress...');

    wpd.save();
    out.success('Initialised!');

    browser();
  });
};
