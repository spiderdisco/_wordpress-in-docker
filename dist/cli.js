#!/usr/bin/env node

const fs = require('fs');
const fse = require('fs-extra');
const inquirer = require('inquirer');
const wpInit = require('./init');
const docker = require('./docker');
const browser = require('./browser');
const { execHost } = require('./utils');
const out = require('./out');

const wpd = require('./wpd');

const program = require('commander')
  .version(require('../package.json').version)
;


///////////////////////////////////////////////////////////////////////////////
// Init
///////////////////////////////////////////////////////////////////////////////
program.command('init')
  .description('Initialise a wordpress-in-docker project.')

  .option('-t, --theme <name>', 'Create a blank WordPress theme with this name.')

  .action((options) => {
    console.log(options.theme);

    if(wpd.isInitialised) {
      out.error('Already appears to be initialised.');
      process.exit(1);
    }

    if(options) {
      wpd.themeName = options.theme;
    }

    const isDirectoryEmpty = fs.readdirSync(wpd.directory).length === 0;

    (async () => {
      // Confirm if the directory is not empty.
      if(!isDirectoryEmpty) {
        await inquirer.prompt([{
          type: 'confirm',
          name: 'confirm',
          message: `${wpd.directory} is not empty. Continue?`
        }]).then(answers => {
          if(!answers.confirm) {
            process.exit();
          }
        });
      }

      // Prompt for any missing information.
      const questions = [];
      if(!wpd.hasTheme) {
        questions.push({
          type: 'input',
          name: 'themeName',
          message: 'Enter theme name (leave blank to skip creation of theme):'
        });
      }
      if(questions) {
        await inquirer.prompt(questions).then(answers => {
          if(answers.themeName) {
            wpd.themeName = answers.themeName;
          }
        });
      }

      // Initialise.
      wpInit();
    })();


  })
;

///////////////////////////////////////////////////////////////////////////////
// Up
///////////////////////////////////////////////////////////////////////////////
program.command('up')
  .description('Start up an existing wordpress-in-docker setup')
  .action(() => {
    docker.up().then(browser);
  })
;


///////////////////////////////////////////////////////////////////////////////
// Shell
///////////////////////////////////////////////////////////////////////////////
program.command('shell')
  .description('Spawn an interactive shell inside the WordPress container.')
  .action(() => {
    out.info('Spawning shell...');
    execHost(`docker exec -it ${wpd.name}_wordpress /bin/bash`);
  })
;


///////////////////////////////////////////////////////////////////////////////
// Export
///////////////////////////////////////////////////////////////////////////////
program.command('export')
  .description('Export WordPress files and/or a database dump from the Docker containers to the host machine.\n\tCalling with no options will export both by default.')
  .option('-d, --database', 'Perform a database dump.')
  .option('-w, --wordpress', 'Create a copy of the WordPress directory.')
  .action((options) => {
    let doWP = options.wordpress;
    let doDB = options.database;
    if(!doWP && !doDB) {
      doWP = true;
      doDB = true;
    }

    if(doWP) {
      out.info('Exporting WordPress...');

      // Clean-up previous exports.
      docker.execWP('rm -rf /tmp/export', true);
      fse.remove('./wp_export/');

      // Copy WordPress directory.
      docker.execWP('cp -r /var/www/html/ /tmp/export/', true);

      // Remove symlink to theme directory, if there is one.
      if(wpd.hasTheme) {
        docker.execWP(`unlink /tmp/export/wp-content/themes/${wpd.safeThemeName}`, true);
      }

      // Copy wp installation
      execHost(`docker container cp ${wpd.name}_wordpress:tmp/export/. ${wpd.directory}/wp_export`);
      docker.execWP('rm -rf /tmp/export', true);

      // Copy the theme directory into the output.
      if(wpd.hasTheme) {
        fse.copySync(wpd.safeThemeName, `${wpd.directory}/wp_export/wp-content/themes/${wpd.safeThemeName}`);
      }
    }

    if(doDB) {
      out.info('Dumping database...');
      docker.execDB('MYSQL_PWD=somewordpress /usr/bin/mysqldump -u root wordpress > dump.sql');
      execHost(`docker cp ${wpd.name}_database:dump.sql ${wpd.directory}`);
    }

    out.success('Exported!');
  })
;

program.parse(process.argv);
