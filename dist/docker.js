const request = require('request');
const cp = require('child_process');
const { spawnHost, execOptions } = require('./utils');
const wpd = require('./wpd');

function up() {
  spawnHost('docker-compose',[
    `--file=${wpd.directory}/docker-compose.yml`,
    'up',
  ]).on('exit', () => process.exit());
  return waitForDocker();
}

/**
 * Execute a command in the WordPress container.
 * @param {string} command command execute
 * @param {boolean} asRoot if true, run the command as root user, else run as www-data.
 */
function execWP(command, asRoot) {
  cp.execSync(
    `docker exec --user ${asRoot ? 'root' : 'www-data'} ${wpd.name}_wordpress sh -c '${command}'`,
    execOptions
  );
}

function wpConfigDefine(key, value) {
  const command = `echo "define( \\"${key}\\", \\"${value}\\" );" >> wp-config.php`;
  cp.execSync(
    `docker exec --user www-data ${wpd.name}_wordpress sh -c '${command}'`,
    execOptions
  );
}

/**
 * Execute a command in the database container.
 * @param {string} command command execute
 */
function execDB(command) {
  cp.execSync(
    `docker exec ${wpd.name}_database sh -c '${command}'`
  );
}

function waitForDocker() {
  return new Promise((resolve, reject) => {

    function tryToConnect(resolve, reject) {
      request('http://localhost:8000', (err, response, body) => {
        if(err || response.statusCode !== 200) {
          setTimeout(() => tryToConnect(resolve, reject), 100);
        } else {
          resolve();
        }
      });
    }
    tryToConnect(resolve, reject);
  });
}

module.exports = {
  up,
  execWP,
  wpConfigDefine,
  execDB,
};
