const colors = require('colors/safe');

function print(icon, message, color) {
  let msg = ` ${icon} ${message}`;
  console.log(color(msg));
}

function tmi(message) {
  print('-', message, colors.dim);
}

function info(message) {
  print('>', message, colors.blue);
}

function success(message) {
  print('✔', message, colors.green);
}

function error(message) {
  print('✖', message, colors.red);
}

module.exports = {
  info, success, tmi, error
};
