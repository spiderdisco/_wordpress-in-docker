****WordPress in Docker****
===========================
This package provides the '`wpd`' command-line utility, which automates a few tedious tasks encountered during local development using WordPress using Docker.

## Features: ##
 - Easy creation of a local WordPress dev environment using Docker.
 - Serve locally via [BrowserSync](https://www.browsersync.io/).
 - Theme development.
 - Export the project & dump the database.


## To-do... ##
WordPress in Docker is a work-in-progress. The following features are planned:
- Plugin development.
- Import existing WordPress

If you have any problems or suggestions, please [create an issue](https://gitlab.com/spiderdisco/wordpress-in-docker/issues)!


# Installation #
```bash
npm install -g wordpress-in-docker
```

You need both **[Docker](https://docs.docker.com/install/)** and **[Docker-Compose](https://docs.docker.com/compose/install/)** installed on your dev machine.

# Guide #
## The famous one command install:
```bash
wpd init
```

Run this in the directory you want to contain your project. You will be prompted to supply a name for your WordPress theme. **wpd** will pull the required Docker images, and configure them for development. This might take a little while the first time you run it.

Once it is ready, you should see the familiar WordPress installation screen appear in your browser. A directory will have been created in your for your theme, containing the minimal boilerplate. BrowserSync is watching this directory, so changes will be automagically pushed to the browser at `localhost:3000`.


## Developing
To continue work an existing project, run
```bash
wpd up
```
to start the Docker containers and BrowserSync. (Note that **wpd** is *not* required to run the containers; `docker-compose up` should work just fine. It will be served from `localhost:8000`.)

If wish to use [WP-CLI](https://wp-cli.org/), you can get a terminal inside of the WordPress container with `wpd shell`, where WP_CLI is available to run as `wp`.


## Export
Once you need to host or share the project, **wpd** lets you conveniently export the WordPress files and database:
```bash
wpd export
```
This will create a database dump in `dump.sql`, and the WordPress files in `wp_export/`.

To export *just* the database, use `wpd export -d`, and to export *just* the WordPress files, use `wpd export -w`.

# CLI #
**Usage:** `wpd [options] [command]`

| Options       | Description                          |
|---------------|--------------------------------------|
|`-V, --version`| output the version number |
|`-h, --help`   | output usage information |

| Commands         | Description                          |
|------------------|--------------------------------------|
|`init [options]`  | Initialise a wordpress-in-docker project. |
|`up`              | Start up an existing wordpress-in-docker setup. |
|`shell`           | Spawn an interactive shell inside the WordPress container. |
|`export [options]`| Export WordPress files and/or a database dump from the Docker containers to the host machine. |

## `init` ##
Initialise a wordpress-in-docker project.

**Usage:** `wpd init [options]`

| Options       | Description                          |
|---------------|--------------------------------------|
|`-t, --theme <name>`| Create a blank WordPress theme with this name. |
|`-h, --help`        | output usage information |


## `up` ##
Start up an existing wordpress-in-docker setup.

**Usage:** `wpd up`

| Options       | Description                          |
|---------------|--------------------------------------|
|`-h, --help`   | output usage information |


## `export` ##
Export WordPress files and/or a database dump from the Docker containers to the host machine.

**Usage:** `wpd export [options]`

| Options       | Description                          |
|---------------|--------------------------------------|
|`-d, --database` | Create a blank WordPress theme with this name. |
|`-w, --wordpress`| Create a copy of the WordPress directory. |
|`-h, --help`     | output usage information |


----
# References #
 - https://docs.docker.com/compose/wordpress/
 - https://neliosoftware.com/blog/local-wordpress-development-with-docker/
 - https://gist.github.com/spalladino/6d981f7b33f6e0afe6bb
 - https://gist.github.com/chuckreynolds/6032234
----
